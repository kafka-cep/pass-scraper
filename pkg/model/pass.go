package model

type Pass struct {
	ID    int `json:"id"`
	Level int `json:"level"`
}
